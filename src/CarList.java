import java.text.DecimalFormat;

/**
 * Created by Dmitry Vereykin on 10/20/2015.
 */
public class CarList {
    ArrayUnsortedList<Car> carList;

    public CarList(){
        carList = new ArrayUnsortedList<>();
    }

    public void addCar(String year, String make, String model, int price) {
        Car newCar = new Car(year, make, model, price);
        carList.add(newCar);
    }

    public int getTotal(){
        DecimalFormat df = new DecimalFormat("#,##0.00");
        int count = 0;
        int total = 0;
        carList.reset();

        while (count != carList.size()){
            //System.out.println("  " + (count + 1) + ". " + carList.getNext().toString());
            System.out.print("  " + (count + 1) + ".  ");
            System.out.printf("%-12s%-15s%-20s%-15s\n", carList.getNext().getYear(), carList.getNext().getMake(),
                    carList.getNext().getModel(),"$" + df.format((double) carList.getNext().getPrice()));
            total = total + carList.getNext().getPrice();
            count++;
        }

        return total;
    }
}
