import java.text.DecimalFormat;

/**
 * Created by Dmitry Vereykin on 10/20/2015.
 */
public class Test {

    public static void main(String[] args) {
        CarList carList = new CarList();
        carList.addCar("2015", "Nissan", "Titan SL", 46270);
        carList.addCar("2016", "Bentley", "Mulsanne Speed", 371611);
        carList.addCar("1963", "Chevrolet", "Corvette", 59999);


        DecimalFormat df = new DecimalFormat("#,##0.00");
        System.out.println("\n    Total:  $" + df.format((double) carList.getTotal()));
    }
}
